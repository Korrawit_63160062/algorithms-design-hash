/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithmdesign_hash;

import java.util.ArrayList;
/**
 *
 * @author Acer
 */
public class HashArrayList {

    int M;
    ArrayList<String> table = new ArrayList<String>();

    public HashArrayList(int M) {

        this.M = M;
        for (int i = 0; i < M; i++) {
            table.add(i, null);
        }

    }

    private int hashFunc(int k) {
        int h = k % M;
        return h;
    }

    public String get(int k) {
        int h = hashFunc(k);
        return table.get(h);
    }

    public void getAll() {
        for (int i = 0; i < M; i++) {
            System.out.println("[ Key : " + i + " , Value : " + table.get(i) + " ]");
        }
    }

    public void put(int k, String v) {
        int h = hashFunc(k);
            table.set(h, v);
    }

    public void remove(int k) {
        int h = hashFunc(k);
        table.set(h, null);
    }

}
