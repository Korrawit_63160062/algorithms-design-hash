package com.mycompany.algorithmdesign_hash;

import java.util.LinkedList;

class Node {

    int key;
    String value;
    Node next;

    Node(int k, String v, Node n) {
        this.key = k;
        this.value = v;
        this.next = n;
    }
}

public class HashLinkedList {

    int M;
    LinkedList<String> table = new LinkedList();
    Node[] node;

    public HashLinkedList(int M) {
        this.M = M;
        for (int i = 0; i < M; i++) {
            table.add(i, null);
        }
        node = new Node[M];
    }

    private int hashFunc(int k) {
        int h = k % M;
        return h;
    }

    public String get(int k) {
        int h = hashFunc(k);
        for (Node i = node[h]; i != null; i = i.next) {
            if (k == i.key) {
                return i.value;
            }
        }
        return "null";
    }

    public void getAll() {
        for (int i = 0; i < M; i++) {
            for (Node j = node[i]; j != null; j = j.next) {
                System.out.println(i + " -> " + j.key + " " + j.value);
            }
        }
    }

    public void put(int k, String v) {
        int h = hashFunc(k);
        for (Node i = node[h]; i != null; i = i.next) {
            if (k == i.key) {
                i.value = v;
                return;
            }
        }
        node[h] = new Node(k, v, node[h]);
    }

    public void remove(int k) {
        int h = hashFunc(k);
        table.set(h, null);
    }

}
