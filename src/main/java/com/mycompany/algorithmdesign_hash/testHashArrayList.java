/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithmdesign_hash;

/**
 *
 * @author Acer
 */
public class testHashArrayList {
    public static void main(String[] args) {
        HashArrayList hash = new HashArrayList(11);
        hash.put(11, "Vernon");
        hash.put(28, "Jissen");
        hash.put(40, "Dino");
        hash.put(22, "Gyuri"); // Vernon replaced
        hash.remove(28); // Jissen deleted
        System.out.println(hash.get(40)); // Print Dino
        hash.getAll();
    }
    
}
